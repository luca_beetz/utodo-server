use diesel;
use diesel::prelude::*;
use crate::schema::todos;

#[table_name = "todos"]
#[derive(Serialize, Deserialize, Queryable, AsChangeset)]
pub struct Todo {
    pub id: i32,
    pub title: String,
    pub content: String,
}

impl Todo {

    pub fn all(connection: &PgConnection) -> QueryResult<Vec<Todo>> {
        todos::table.load::<Todo>(&*connection)
    }

    pub fn get(id: i32, connection: &PgConnection) -> QueryResult<Todo> {
        todos::table.find(id).get_result::<Todo>(connection)
    }

    pub fn insert(todo: InsertableTodo, connection: &PgConnection) -> QueryResult<Todo> {
        diesel::insert_into(todos::table)
            .values(todo)
            .get_result(connection)
    }

    pub fn update(id: i32, todo: Todo, connection: &PgConnection) -> QueryResult<Todo> {
        diesel::update(todos::table.find(id))
            .set(&todo)
            .get_result(connection)
    }

    pub fn delete(id: i32, connection: &PgConnection) -> QueryResult<usize> {
        diesel::delete(todos::table.find(id))
            .execute(connection)
    }

}

#[table_name = "todos"]
#[derive(Insertable, Serialize, Deserialize)]
pub struct InsertableTodo {
    pub title: String,
    pub content: String,
}

impl InsertableTodo {

    fn from_todo(todo: Todo) -> Self {
        InsertableTodo {
            title: todo.title,
            content: todo.content,
        }
    }

}

