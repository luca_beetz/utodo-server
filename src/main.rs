#![feature(proc_macro_hygiene, decl_macro, custom_attribute)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde;
#[macro_use] extern crate rocket_contrib;

#[macro_use] extern crate diesel;

use dotenv;

mod schema;
mod model;
mod api;
mod db;

fn main() {
    dotenv::dotenv().ok();

    api::start_api();
}