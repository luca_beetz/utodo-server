use crate::db;
use crate::db::DbConn;
use crate::model::Todo;
use crate::model::InsertableTodo;
use diesel::result::Error;
use rocket::http::Status;
use rocket::response::status;
use rocket_contrib::json::Json;

#[get("/")]
fn all(connection: DbConn) -> Result<Json<Vec<Todo>>, Status> {
    Todo::all(&connection)
        .map(|todos| Json(todos))
        .map_err(|err| error_status(err))
}

#[get("/<id>")]
fn get(id: i32, connection: DbConn) -> Result<Json<Todo>, Status> {
    Todo::get(id, &connection)
        .map(|todo| Json(todo))
        .map_err(|err| error_status(err))
}


#[post("/", format = "application/json", data = "<todo>")]
fn post(todo: Json<InsertableTodo>, connection: DbConn) -> Result<status::Created<Json<Todo>>, Status> {
    Todo::insert(todo.into_inner(), &connection)
        .map(|todo| status::Created("Created".to_string(), Some(Json(todo))))
        .map_err(|err| error_status(err))
}

#[put("/<id>", format = "application/json", data = "<todo>")]
fn put(id: i32, todo: Json<Todo>, connection: DbConn) -> Result<Json<Todo>, Status> {
    Todo::update(id, todo.into_inner(), &connection)
        .map(|todo| Json(todo))
        .map_err(|err| error_status(err))
}

#[delete("/<id>")]
fn delete(id: i32, connection: DbConn) -> Result<status::Accepted<&'static str>, Status> {
    match Todo::get(id, &connection) {
        Ok(_) => {
            Todo::delete(id, &connection)
                .map(|count| status::Accepted(Some("Deleted")))
                .map_err(|err| error_status(err))
        }
        Err(err) => Err(error_status(err))
    }
}

fn error_status(error: Error) -> Status {
    match error {
        Error::NotFound => Status::NotFound,
        _ => Status::InternalServerError
    }
}

pub fn start_api() {
    rocket::ignite()
        .manage(db::init_pool())
        .mount("/todos",
            routes![
                all,
                get,
                put,
                delete,
                post
            ]
        ).launch();
}
